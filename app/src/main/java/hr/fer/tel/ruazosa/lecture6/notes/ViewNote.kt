package hr.fer.tel.ruazosa.lecture6.notes

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_view_note.*

/**
 * Created by Dominik on 5/26/2018.
 */
class ViewNote : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_note)

        val (title, description) = NoteDescription
        description_note.text = description
        title_note.text = title
    }

}