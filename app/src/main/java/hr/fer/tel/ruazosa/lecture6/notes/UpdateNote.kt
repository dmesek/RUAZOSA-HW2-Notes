package hr.fer.tel.ruazosa.lecture6.notes

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_update_note.*
import android.provider.SyncStateContract.Helpers.update
import com.j256.ormlite.stmt.UpdateBuilder
import kotlin.concurrent.thread


/**
 * Created by Dominik on 5/27/2018.
 */
class UpdateNote : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_note)

        val (title, description) = NoteDescription

        note_title.text = title
        note_description.setText(description, TextView.BufferType.EDITABLE);

        cancel_button.setOnClickListener{
            finish()
        }

        update_button.setOnClickListener{
            val tableDao = DatabaseHelper(this).getDao(NotesModel.Note::class.java)
            val updateBuilder = tableDao.updateBuilder()
            updateBuilder.where().eq("noteTitle", title)
            updateBuilder.updateColumnValue("noteDescription", note_description.text.toString())
            updateBuilder.update()

            NotesModel.notesList = sortedSetOf(compareByDescending { note -> note.noteTime })
            NotesModel.notesList.addAll(tableDao.queryForAll())

            finish()
        }



    }


}