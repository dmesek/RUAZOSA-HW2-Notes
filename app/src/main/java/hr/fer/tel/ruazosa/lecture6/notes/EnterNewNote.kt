package hr.fer.tel.ruazosa.lecture6.notes

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.thread

class EnterNewNote : AppCompatActivity() {

    private var noteTitle: EditText? = null
    private var noteDescription: EditText? = null
    private var storeButton: Button? = null
    private var cancelButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_new_note)

        noteTitle = findViewById(R.id.note_title)
        noteDescription = findViewById(R.id.note_description)

        storeButton = findViewById(R.id.store_button)
        storeButton?.setOnClickListener({
            if(noteTitle?.text.toString() == "")
                Toast.makeText(this, "Please type in a title", Toast.LENGTH_SHORT).show()
            if(noteDescription?.text.toString() == "")
                Toast.makeText(this, "Please type in a description", Toast.LENGTH_SHORT).show()
            else {
                val note = NotesModel.Note(noteTitle = noteTitle?.text.toString(),
                        noteDescription = noteDescription?.text.toString(), noteTime = Date())
                val tableDao = DatabaseHelper(this).getDao(NotesModel.Note::class.java)
                tableDao.create(note)

                NotesModel.notesList.add(note)

                finish()
            }
        })
        cancelButton = findViewById(R.id.cancel_button)

        cancelButton?.setOnClickListener({
            finish()
        })
    }
}
