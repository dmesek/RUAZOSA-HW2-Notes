package hr.fer.tel.ruazosa.lecture6.notes

/**
 * Created by Dominik on 5/26/2018.
 */
object NoteDescription {
    var noteTitle : String? = null
    var noteDesc : String? = null

    operator fun component1() : String?{
        return noteTitle
    }

    operator fun component2() : String?{
        return noteDesc
    }
}