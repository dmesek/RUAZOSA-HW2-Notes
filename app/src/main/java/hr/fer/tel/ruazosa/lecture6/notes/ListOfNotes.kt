package hr.fer.tel.ruazosa.lecture6.notes

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import java.text.SimpleDateFormat
import kotlin.concurrent.thread
import android.content.DialogInterface



class ListOfNotes : AppCompatActivity() {

    private var fab: FloatingActionButton? = null
    private var listView: ListView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_of_notes)
        fab = findViewById(R.id.fab)

        fab?.setOnClickListener({
            val startEnterNewNoteIntent = Intent(this, EnterNewNote::class.java)
            startActivity(startEnterNewNoteIntent)
        })

        listView = findViewById(R.id.list_view)
        listView?.adapter = NotesAdapter(this)
    }

    inner class NotesAdapter(context: Context) : BaseAdapter() {

        private var notesList = NotesModel.notesList
        private var context: Context? = context


        @SuppressLint("SimpleDateFormat")
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

            val view: View?
            val vh: ViewHolder

            if (convertView == null) {
                view = layoutInflater.inflate(R.layout.note_in_list, parent, false)
                vh = ViewHolder(view)
                view.tag = vh
            } else {
                view = convertView
                vh = view.tag as ViewHolder
            }
            val dateFormatter = SimpleDateFormat("dd'.'MM'.'yyyy ',' HH:mm");


            val dateAsString = dateFormatter.format(notesList.elementAt(position).noteTime)
            vh.noteTitle.text = notesList.elementAt(position).noteTitle
            vh.noteTime.text = dateAsString
            return view
        }

        override fun getItem(position: Int): Any {
            return notesList.elementAt(position)
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return notesList.size
        }

        init {
            val tableDao = DatabaseHelper(this@ListOfNotes).getDao(NotesModel.Note::class.java)
            NotesModel.notesList.addAll(tableDao.queryForAll())
        }
    }

    inner class ViewHolder(view: View?){
        val noteTime: TextView
        val noteTitle: TextView
        val delButton: ImageButton
        val editButton : ImageButton
        val layout : LinearLayout

        init {
            this.noteTime = view?.findViewById<TextView>(R.id.note_time) as TextView
            this.noteTitle = view.findViewById<TextView>(R.id.note_title) as TextView
            this.delButton = view.findViewById<ImageButton>(R.id.deleteButton) as ImageButton
            this.layout = view.findViewById<LinearLayout>(R.id.clickable_layout) as LinearLayout
            this.editButton = view.findViewById<ImageButton>(R.id.editButton) as ImageButton

            delButton.setOnClickListener{
                val builder = AlertDialog.Builder(this@ListOfNotes)

                builder.setMessage("Are you sure you want to delete " + noteTitle.text.toString() + "?")

                builder.setPositiveButton("YES", DialogInterface.OnClickListener { dialog, which ->
                    val tableDao = DatabaseHelper(this@ListOfNotes).getDao(NotesModel.Note::class.java)
                    val deleteBuilder = tableDao.deleteBuilder();
                    deleteBuilder.where().eq("noteTitle", noteTitle.text.toString());
                    thread {
                        deleteBuilder.delete();
                    }
                    NotesModel.notesList = sortedSetOf(compareByDescending { note -> note.noteTime })
                    finish();
                    startActivity(getIntent());
                })

                builder.setNegativeButton("NO", DialogInterface.OnClickListener { dialog, which ->
                    dialog.dismiss()
                })

                builder.create().show()

            }

            editButton.setOnClickListener{
                val startIntent = Intent(this@ListOfNotes, UpdateNote::class.java)
                NoteDescription.noteTitle = noteTitle.text.toString()
                NoteDescription.noteDesc = NotesModel.notesList
                                                        .find { note -> note.noteTitle == noteTitle.text.toString()  }
                                                        ?.noteDescription
                startActivity(startIntent)
            }

            layout.setOnClickListener{
                val startIntent = Intent(this@ListOfNotes, ViewNote::class.java)
                NoteDescription.noteTitle = noteTitle.text.toString()
                NoteDescription.noteDesc = NotesModel.notesList
                                                    .find { note -> note.noteTitle == noteTitle.text.toString()  }
                                                    ?.noteDescription
                startActivity(startIntent)
            }

        }

        //  if you target API 26, you should change to:
//        init {
//            this.tvTitle = view?.findViewById<TextView>(R.id.tvTitle) as TextView
//            this.tvContent = view?.findViewById<TextView>(R.id.tvContent) as TextView
//        }
    }
}
